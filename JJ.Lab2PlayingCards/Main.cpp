
// Lab Exercise 2 - Playing Cards
// Jason Jeffers

#include <iostream>
#include <conio.h>

using namespace std;

// rank enum
enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
// suit enum
enum Suit { Spades = 1, Clubs, Diamonds, Hearts };

// card struct
struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{


	(void)_getch();
	return 0;
}
